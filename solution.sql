Francis Kyle G. Cabato BSIT-3

a. List the books Authored by Marjorie Green
- The Busy Executive's Database Guide
- You can Combat Computer Stress!
b. List the books Authored by Micheal O'Leary
- Cooking with Computers
- Sushi, Anyone
c. Write the author/s of "The Busy Executives Database Guide"
-Marjorie Green
d. Identify the publisher of "But Is It User Friendly?"
-Algodata Infosystems	
e. List the books published by Algodata Infosystems
-But Is It User Friendly?
-Secrets of Silicon Valley
-Net Etiquette
-The Busy Executive's Database Guide
-Cooking with Computers
-Straight Talk About Computers
------------------------------------------------------------------------

Microsoft Windows [Version 10.0.19044.2604]
(c) Microsoft Corporation. All rights reserved.

C:\Users\Francis Kyle>mysql -u root
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 5.5.5-10.4.24-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> CREATE DATABASE blog_db;
Query OK, 1 row affected (0.00 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| admissions         |
| blog_db            |
| dbeventsystem      |
| information_schema |
| music_db           |
| mysql              |
| nursingunits       |
| patient            |
| performance_schema |
| phpmyadmin         |
| quiz               |
| restaurant         |
| sample             |
| test               |
+--------------------+
14 rows in set (0.00 sec)

mysql> CREATE TABLE post (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id));
ERROR 1046 (3D000): No database selected
mysql> USE DATABASE blog_db;
ERROR 1049 (42000): Unknown database 'database'
mysql> USE blog_db;
Database changed
mysql> CREATE TABLE post (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id));
Query OK, 0 rows affected (0.03 sec)

mysql> ALTER TABLE post RENAME TO posts;
Query OK, 0 rows affected (0.02 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| admissions         |
| blog_db            |
| dbeventsystem      |
| information_schema |
| music_db           |
| mysql              |
| nursingunits       |
| patient            |
| performance_schema |
| phpmyadmin         |
| quiz               |
| restaurant         |
| sample             |
| test               |
+--------------------+
14 rows in set (0.00 sec)

mysql> SHOW TABLES'
    '> ;
    '> SHOW TABLES;
    '> DFDS
    '>
    '>
    '>
    '> DDDS
    '> SHOW TABLES'
    ->
    -> SHOW DATABASES;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''
;
SHOW TABLES;
DFDS



DDDS
SHOW TABLES'

SHOW DATABASES' at line 1
mysql> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
+-------------------+
1 row in set (0.00 sec)

mysql> CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME, PRIMARY KEY (id));
Query OK, 0 rows affected (0.03 sec)

mysql> DROP TABLE posts;
Query OK, 0 rows affected (0.01 sec)

mysql> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| users             |
+-------------------+
1 row in set (0.00 sec)

mysql> CREATE TABLE posts (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id), CONSTRAINT fk_author_id, FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ' FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE REST...' at line 1
mysql> CREATE TABLE posts (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id), CONSTRAINT fk_author_id FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.03 sec)

mysql> DESCRIBE TABLES;
ERROR 1146 (42S02): Table 'blog_db.tables' doesn't exist
mysql> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)

mysql> CREATE TABLE post_comments ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL, content VARCHAR(5000) NOT NULL,datetime_commented DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.03 sec)

mysql> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
ERROR 1005 (HY000): Can't create table `blog_db`.`post_likes` (errno: 121 "Duplicate key on write or update")
mysql> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);  CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE RESTRICT ON UPDATE CASCADE,
ERROR 1005 (HY000): Can't create table `blog_db`.`post_likes` (errno: 121 "Duplicate key on write or update")
    ->     CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE
    ->
    -> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE RE...' at line 1
mysql> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE DELETE RESTRICT ON UPDATE CASCADE, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'DELETE RESTRICT ON UPDATE CASCADE, CONSTRAINT fk_user_id FOREIGN KEY (user_id...' at line 1
mysql> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
ERROR 1005 (HY000): Can't create table `blog_db`.`post_likes` (errno: 121 "Duplicate key on write or update")
mysql> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_likes_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.03 sec)

mysql> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.00 sec)

mysql>